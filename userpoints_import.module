<?php
    
/*
* Implements hook menu
*/
function userpoints_import_menu() {
  $items['admin/config/people/userpoints/import'] = array(
    'title' => '!Points import',
    'title arguments' => userpoints_translation(),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('userpoints_import_form'),
    'access arguments' => array('userpoints_import'),
    'type' => MENU_LOCAL_ACTION,
  );
  return $items;
}

/**
 * Provides administrative interface for importing userpoints transactions.
 * @param type $form
 * @param type $form_state
 */
function userpoints_import_form($form, &$form_state) {
    $description = 'Copy and paste data for input from CSV file. '
            . 'One record per line in this format: '
            . 'username points transaction_description '
            . '(tab separated CSV format).';
    $form['csv_data'] = array(
        '#type' => 'textarea',
        '#title' => t('Transactions for import'),
        '#description' => t($description),
        '#cols' => 20,
        '#required' => TRUE,
    );
    $form['submit'] = array(
        '#type'  => 'submit',
        '#value' => t('Import'),
    );
    return $form;
}

/**
 * Processing of submitted date for import.
 * @param type $form
 * @param type $form_state
 */
function userpoints_import_form_submit($form, &$form_state) {
    $data = userpoints_import_prepare_data($form_state['values']['csv_data']);
    userpoints_import_save_data($data);
}

/**
 * Parese uploaded text data to associative data array.
 * @param type $data
 * @return type
 */
function userpoints_import_prepare_data($data) {
    $return_data = array();
    $lines = explode("\n", $data);
    foreach ($lines as $idx => $line) {
        if ($line) {
            list($username, $points, $description) = explode("\t", $line, 3);
            $return_data[$idx] = array(
              'username' => $username,
              'points' => intval($points),
              'description' => $description,
            );
        }
    }
    return $return_data;
}

/**
 * Provides saving prepared data.
 * @param type $data
 */
function userpoints_import_save_data($data) {
    foreach ($data as $row) {
        $account = user_load_by_name(check_plain($row['username']));
        if (!$account) {
                drupal_set_message(t("User with name @username isn't exist.", 
                    array('@username' => $row['username'])), 'warning');
            continue;
        }
        $params = array (
          'tid' => variable_get('userpoints_import_category', 0),
          'uid' => $account->uid,
          'points' => $row['points'],
          'operation' => 'userpoints_import',
          'description' => is_null($row['description'])? t('Import points'): $row['description'],
          'moderate' => variable_get('userpoints_import_moderate', 0),
        );
        userpoints_userpointsapi($params);
    }
}

/**
 * Implementation hook_userpoints_info
 */
function userpoints_import_userpoints_info() {
  return array(
    'userpoints_import' => array(
      'description' => t('!Points have been imported.', userpoints_translation()),
      'admin description' => t('Import transaction from CSV file.'),
    )
  );
}
